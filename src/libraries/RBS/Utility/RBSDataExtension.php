<?php
namespace RBS\Utility;
use RBS\Selifa\Data\DataObject;
use RBS\Selifa\Data\DataRow;
use RBS\Selifa\Data\IAuditedDataExtension;
use RBS\Selifa\Data\IDataExtension;
use DateTime;

/**
 * Class RBSDataExtension
 * @package RBS\Utility
 */
class RBSDataExtension implements IDataExtension, IAuditedDataExtension
{
    /**
     * @var int
     */
    private $_UserID = 0;

    /**
     *
     */
    public function __construct()
    {

    }

    /**
     * @return array|null
     */
    public function GetInitialData()
    {
        return array();
    }

    /**
     * @param int $userId
     */
    public function SetUserID($userId)
    {
        $this->_UserID = $userId;
    }

    /**
     * @return int
     */
    public function GetUserID()
    {
        return $this->_UserID;
    }

    /**
     * @param DataRow $newRow
     * @return mixed|null
     */
    public function NewRowCreated($newRow)
    {
        // TODO: Implement NewRowCreated() method.
    }

    /**
     * @param DataObject $dataObject
     * @param array $filters
     * @return mixed
     */
    public function FilterRow($dataObject, &$filters)
    {
        $filters['DFlag'] = 0;
    }

    /**
     * @param DataRow $dbRow
     * @return mixed
     */
    public function RowFetched($dbRow)
    {
        // TODO: Implement RowFetched() method.
    }

    /**
     * @param DataRow $dbRow
     * @param int $rowMode
     * @param bool $cancelSave
     */
    public function SaveRow($dbRow, $rowMode, &$cancelSave)
    {
        if ($rowMode == DB_ROW_MODE_INSERT)
        {
            $dbRow->CUID = $this->_UserID;
            $dbRow->MUID = $this->_UserID;
            $dbRow->Created = new DateTime();
        }
        else if ($rowMode == DB_ROW_MODE_UPDATE)
        {
            $dbRow->MUID = $this->_UserID;
        }
    }

    /**
     * @param DataRow $dbRow
     * @param int $rowMode
     */
    public function RowSaved($dbRow, $rowMode)
    {
        // TODO: Implement RowSaved() method.
    }

    /**
     * @param DataRow $dbRow
     * @param bool $cancelDelete
     * @param bool $doUpdateInstead
     */
    public function DeleteRow($dbRow, &$cancelDelete, &$doUpdateInstead)
    {
        $dbRow->MUID = $this->_UserID;
        $dbRow->DFlag = 1;
        $doUpdateInstead = true;
    }

    /**
     * @param DataRow $dbRow
     * @return mixed
     */
    public function RowDeleted($dbRow)
    {
        // TODO: Implement RowDeleted() method.
    }

    /**
     * @return string
     */
    public function GetSoftDeleteColumnKey()
    {
        return 'DFlag';
    }
}
?>