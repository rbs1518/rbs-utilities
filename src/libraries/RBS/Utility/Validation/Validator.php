<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation;
use Exception;
use RBS\Selifa\Template\TGX;
use RBS\System\ITransferableObject;

define('VALIDATOR_RULE_AND_DATA',0);
define('VALIDATOR_ONLY_RULE',1);
define('VALIDATOR_ONLY_DATA',2);

/**
 * Validator utility component.
 *
 * How To Use:
 *      Create validator object with its rule as parameter.
 *          $validator = new Validator(<array_or_rule>);
 *
 *      How to define rules: simple, similar to Yii2 rule definition (similar but not same).
 *          [
 *              [<property_name>,<validator>],
 *              [[array_of_property_name>],<validator>]
 *              [<property_name>,[<array_of_validator>],
 *              [
 *                  <property_name>,
 *                  <validator>,
 *                  'when' => <callable or object of IValidationCondition>,
 *                  'message' => <string of error message>,
 *                  'each' => <true/false, specify whether to validate each item in array if property's value is an array>
 *              ]
 *          ]
 *
 *      Each validator can have parameters, how to define them?
 *          <validator> => <parameter> or [<array_of_parameter>]
 *      Custom error message defined in 'message' can have template key {!key} for property name, and {!value} for property's value.
 *
 *      For validator's parameters, refer to each validator component in RBS\Utility\Validation\Component namespace.
 *
 *      To implement custom validator, create a class that implements IValidator interface,
 *          and to use it, use <custom_validator_full_classname> in <validator>, or use its object in <validator>.
 *
 *      After all neccesary rules are defined, then use this example to actually validate the data.
 *          $validator->Validate($data);
 *
 *      Validator's object can be used multiple times against different data.
 *
 * @package RBS\Utility\Validation
 */
class Validator
{
    /**
     * @var null|array
     */
    private $_Rules = null;

    /**
     * @var array
     */
    private $_PreparedRules = [];

    /**
     * @var int
     */
    private $_Behaviour = VALIDATOR_RULE_AND_DATA;

    /**
     * @param string|object $keyOrClassName
     * @return IValidator
     * @throws Exception
     */
    protected function GetValidatorObject($keyOrClassName)
    {
        if (is_object($keyOrClassName))
            $obj = $keyOrClassName;
        else if (strpos($keyOrClassName,"\\",0) > -1)
            $obj = new $keyOrClassName();
        else
        {
            $nsPrefix = "RBS\\Utility\\Validation\\Component\\";
            $fqcn = ($nsPrefix.$keyOrClassName.'Validator');
            $obj = new $fqcn();
        }

        if ($obj instanceof IValidator)
            return $obj;
        else
            throw new Exception("\"".get_class($obj)."\" is not an instance of IValidator.");
    }

    /**
     * @param array $rules
     * @return array
     * @throws Exception
     */
    protected function PrepareRules($rules)
    {
        //var_dump($rules);
        $final = [];
        foreach ($rules as $ruleKey => $ruleItem)
        {
            if (!isset($ruleItem[0]))
                continue;

            if (!isset($ruleItem[1]))
                continue;

            $props = $ruleItem[0];
            $validators = $ruleItem[1];

            if (!is_array($props))
                $props = [$props];

            if (!is_array($validators))
                $validators = [$validators];

            $fVldtrs = [];
            foreach ($validators as $vIdx => $vSig)
            {
                $vFunc = null;
                $vObjName = '';
                $vParams = null;
                if (is_string($vIdx))
                {
                    $vObjName = strtolower(trim($vIdx));
                    if (!is_array($vSig))
                        $vParams = [$vSig];
                    else
                        $vParams = $vSig;
                }
                else
                {
                    if (is_callable($vSig))
                        $vFunc = $vSig;
                    else
                        $vObjName = strtolower(trim($vSig));
                    $vParams = [];
                }

                if ($vFunc !== null)
                    $fVldtrs[] = $vFunc;
                else
                {
                    $vObj = $this->GetValidatorObject($vObjName);
                    $vObj->Initialize($vParams);
                    $fVldtrs[] = $vObj;
                }
            }

            $fCondition = null;
            if (isset($ruleItem['when']))
            {
                if (is_callable($ruleItem['when']))
                    $fCondition = $ruleItem['when'];
                else if (is_object($ruleItem['when']))
                {
                    if ($ruleItem['when'] instanceof IValidationCondition)
                        $fCondition = $ruleItem['when'];
                }
                else if (is_string($ruleItem['when']))
                {
                    $fCondition = new $ruleItem['when']();
                    if (!($fCondition instanceof IValidationCondition))
                        throw new Exception("\"".get_class($fCondition)."\" is not an instance of IValidationCondition.");
                }
            }

            $fMessage = '';
            if (isset($ruleItem['message']))
                $fMessage = trim($ruleItem['message']);

            $eachArrayItem = false;
            if (isset($ruleItem['each']))
                $eachArrayItem = (bool)$ruleItem['each'];

            foreach ($props as $propKey)
            {
                $spec = [
                    'id' => (is_string($ruleKey)?trim($ruleKey):''),
                    'prop' => trim($propKey),
                    'validator' => $fVldtrs,
                    'cond' => $fCondition,
                    'message' => $fMessage,
                    'each' => $eachArrayItem
                ];
                $final[$spec['prop']] = $spec;
            }
        }

        //var_dump($final);
        return $final;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param array $rule
     * @param array $data
     * @param mixed|null $bData
     * @return mixed
     * @throws Exception
     */
    protected function ValidateRuleItem($key,$value,$rule,$data,$bData=null)
    {
        $cVal = $value;
        foreach ($rule['validator'] as $validator)
        {
            $forError = [
                'key' => $key,
                'value' => $cVal
            ];

            if (is_callable($validator))
            {
                $cVal = $validator($cVal,$key,$data);
                if ($cVal === null)
                {
                    if ($rule['message'] != '')
                        $cMessage = $rule['message'];
                    else
                        $cMessage = 'Unspecified failed validation message.';
                    $fMsg = TGX::Parse($cMessage,$forError);
                    throw new Exception($fMsg);
                }
            }
            else if ($validator instanceof IValidator)
            {
                if ($validator instanceof IValidatorBoundObject)
                    $validator->SetBoundObject($bData);
                $cVal = $validator->Validate($cVal,$key,$data);
                if ($cVal === null)
                {
                    if ($validator instanceof IValidatorAllowNull)
                    {
                        if ($validator->IsError())
                        {
                            if ($rule['message'] != '')
                                $cMessage = $rule['message'];
                            else
                                $cMessage = $validator->GetMessage('en');
                            $fMsg = TGX::Parse($cMessage,$forError);
                            throw new Exception($fMsg);
                        }
                        else
                        {
                            $doSkipNext = $validator->IsSkipNext();
                            if ($doSkipNext)
                                break;
                        }
                    }
                    else
                    {
                        if ($rule['message'] != '')
                            $cMessage = $rule['message'];
                        else
                            $cMessage = $validator->GetMessage('en');
                        $fMsg = TGX::Parse($cMessage,$forError);
                        throw new Exception($fMsg);
                    }
                }
            }
        }
        return $cVal;
    }

    /**
     * @param array $final
     * @param array $rule
     * @param mixed|null $value
     * @param string $key
     * @param array $data
     * @param mixed|null $bData
     * @throws Exception
     */
    protected function ValidateRule(&$final,$rule,$value,$key,$data,$bData=null)
    {
        $proceed = true;
        if ($rule['cond'] !== null)
        {
            if (is_callable($rule['cond']))
            {
                $condFunc = $rule['cond'];
                $proceed = $condFunc($value,$key,$data);
            }
            else if ($rule['cond'] instanceof IValidationCondition)
            {
                if ($rule['cond'] instanceof IValidatorBoundObject)
                    $rule['cond']->SetBoundObject($bData);
                $proceed = $rule['cond']->Evaluate($value,$key,$data);
            }
        }

        if ($proceed === true)
        {
            if ($rule['each'])
            {
                $cVal = [];
                foreach ($value as $item)
                    $cVal[] = $this->ValidateRuleItem($key,$item,$rule,$data,$bData);
            }
            else
            {
                $cVal = $this->ValidateRuleItem($key,$value,$rule,$data,$bData);
            }
            $final[$key] = $cVal;
        }
        else if ($proceed === false)
            $final[$key] = $value;
    }

    /**
     * Validator constructor.
     *
     * @param array $rules
     * @param int $behaviour
     * @throws Exception
     */
    public function __construct($rules,$behaviour=VALIDATOR_RULE_AND_DATA)
    {
        $this->_Rules = $rules;
        $this->_Behaviour = $behaviour;
        $this->_PreparedRules = $this->PrepareRules($rules);
    }

    /**
     * @param array|ITransferableObject $data
     * @param mixed|null $bData
     * @return array
     * @throws Exception
     */
    public function Validate($data,$bData=null)
    {
        $final = [];
        $pRules = $this->_PreparedRules;

        if ($data instanceof ITransferableObject)
            $data = $data->ToArray();

        if (in_array($this->_Behaviour,[VALIDATOR_RULE_AND_DATA,VALIDATOR_ONLY_DATA]))
        {
            foreach ($data as $key => $value)
            {
                if (isset($pRules[$key]))
                {
                    $rule = $pRules[$key];
                    $this->ValidateRule($final,$rule,$value,$key,$data,$bData);
                    unset($pRules[$key]);
                }
                else
                    $final[$key] = $value;
            }
        }

        if (in_array($this->_Behaviour,[VALIDATOR_RULE_AND_DATA,VALIDATOR_ONLY_RULE]))
        {
            foreach ($pRules as $key => $rule)
            {
                $value = null;
                $this->ValidateRule($final,$rule,$value,$key,$data,$bData);
            }
        }

        return $final;
    }

    /**
     * @return array
     */
    public function GenerateAPISpec()
    {
        $specs = [
            'name' => '',
            'type' => 'object',
            'reqs' => [],
            'props' => []
        ];

        foreach ($this->_Rules as $item)
        {
            if (is_array($item))
            {
                $keyName = '';
                if (count($item) > 0)
                    $keyName = trim($item[0]);

                if (($keyName != '') && (count($item) > 1))
                {
                    if (is_array($item[1]))
                    {
                        if (in_array('required',$item[1]))
                            $specs['reqs'][] = $keyName;

                        if (in_array('integer',$item[1]))
                            $specs['props'][$keyName] = ['type' => 'int', 'length' => 10];
                        else if (in_array('double',$item[1]) || in_array('float',$item[1]))
                            $specs['props'][$keyName] = ['type' => 'real', 'length' => 10];
                        else if (in_array('boolean',$item[1]))
                            $specs['props'][$keyName] = ['type' => 'bool'];
                        else
                            $specs['props'][$keyName] = ['type' => 'string'];
                    }
                    else if (is_string($item[0]))
                    {
                        if ($item[0] == 'required')
                            $specs['reqs'][] = $keyName;
                        $specs['props'][$keyName] = ['type' => 'string'];
                    }
                }
            }
            else if (is_string($item))
                $specs['props'][$item] = ['type' => 'string'];
        }

        return $specs;
    }
}
?>