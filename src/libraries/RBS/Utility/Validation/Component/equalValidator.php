<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\IValidator;
use Exception;

/**
 * Class equalValidator
 *
 * Supported parameter:
 * [<target_key>,<strict>,'target' => <target_key>, 'strict' => <strict>]
 * Where
 *      <target_key> is the key or property name from the data to be compared with current key's value.
 *      <strict> (boolean, default false) is to specify whether to use strict comparator (===) or standard (==).
 *
 *
 * @package RBS\Utility\Validation\Component
 */
class equalValidator implements IValidator
{
    /**
     * @var string
     */
    private $_TargetKey = '';

    /**
     * @var bool
     */
    private $_IsStrict = false;

    /**
     * @param mixed[] $params
     * @throws Exception
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_TargetKey = trim($params[0]);
        else if (isset($params['target']))
            $this->_TargetKey = trim($params['target']);
        else
            throw new Exception('No target key defined for equalValidator.');

        if (isset($params[1]))
            $this->_IsStrict = (bool)$params[1];
        else if (isset($params['strict']))
            $this->_IsStrict = (bool)$params['strict'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if (isset($values[$this->_TargetKey]))
            $tValue = $values[$this->_TargetKey];
        else
            $tValue = null;

        if ($this->_IsStrict)
        {
            if ($value === $tValue)
                return $value;
            else
                return null;
        }
        else
        {
            if ($value == $tValue)
                return $value;
            else
                return null;
        }
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return "{!key}'s value is not equal with ".$this->_TargetKey."'s.";
    }
}
?>