<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\IValidator;

/**
 * Class integerValidator
 *
 * Supported parameter:
 * [<convert>,'convert' => <convert>]
 * Where
 *      <convert> (boolean, default false) specify whether to convert value to integer or only to validate.
 *
 *
 * @package RBS\Utility\Validation\Component
 */
class integerValidator implements IValidator
{
    /**
     * @var bool
     */
    private $_doConvert = false;

    /**
     * @param mixed[] $params
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_doConvert = (bool)$params[0];
        else if (isset($params['convert']))
            $this->_doConvert = (bool)$params['convert'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if ($this->_doConvert)
            return (int)$value;
        else
        {
            if (is_integer($value))
                return $value;
            else
                return null;
        }
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return '{!key} is not an integer.';
    }
}
?>