<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\IValidator;

/**
 * Class hashValidator
 *
 * Supported parameter:
 * [<algo>,'algo' => <algo>]
 * Where
 *      <algo> is the algorithm name for hash function.
 *
 * This validator does not validate the value, but to create a hash from current value.
 *
 * @package RBS\Utility\Validation\Component
 */
class hashValidator implements IValidator
{
    /**
     * @var string
     */
    private $_Algo = 'md5';

    /**
     * @param mixed[] $params
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_Algo = trim($params[0]);
        else if (isset($params['algo']))
            $this->_Algo = trim($params['algo']);
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        return hash($this->_Algo,$values,false);
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return '';
    }
}
?>