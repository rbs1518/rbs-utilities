<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\IValidator;
use RBS\Utility\Validation\IValidatorAllowNull;

/**
 * Class defaultValidator
 *
 * Supported parameter:
 * [<value>,'value' => <value>,'skip_next' => <skip_next:boolean>]
 * Where
 *      <value> is the default value if the specified key or property does not exist in data array.
 *      <skip_next> (boolean, default false) is to specify whether to skip next validation(s) (if any) or not.
 *
 *
 * @package RBS\Utility\Validation\Component
 */
class defaultValidator implements IValidator, IValidatorAllowNull
{
    /**
     * @var mixed|null
     */
    private $_DefaultValue = null;

    /**
     * @var bool
     */
    private $_IsSkipNext = false;

    /**
     * @param mixed[] $params
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_DefaultValue = $params[0];
        else if (isset($params['value']))
            $this->_DefaultValue = $params['value'];

        if (isset($params['skip_next']))
            $this->_IsSkipNext = (bool)$params['skip_next'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if ($value === null)
            return $this->_DefaultValue;
        else if (!isset($values[$key]))
            return $this->_DefaultValue;
        else
        {
            $this->_IsSkipNext = false;
            return $values[$key];
        }
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return '';
    }

    /**
     * @return bool
     */
    public function IsSkipNext()
    {
        return $this->_IsSkipNext;
    }

    /**
     * @return bool
     */
    public function IsError()
    {
        return false;
    }
}
?>