<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Data;
use Exception;
use RBS\Utility\Validation\Validator;

/**
 * Class SpecificationHandlerForCRUDController
 *
 * @package RBS\Utility\Data
 */
trait SpecificationHandlerForCRUDController
{
    public function _spec_action_get_data($params)
    {
        if (!isset($params['linked-model']))
            return [];

        $linkedModel = trim($params['linked-model']);
        if (!is_subclass_of($linkedModel,"RBS\\Selifa\\Data\\AbstractModel"))
            throw new Exception("'".$linkedModel."' does not inherit from AbstractModel.");
        if (!is_subclass_of($linkedModel,"RBS\\Selifa\\Data\\IStaticGetRows"))
            throw new Exception("'".$linkedModel."' does not implements IStaticGetRows.");
        $data = call_user_func([$linkedModel,'GetRows'],[],[],0,-1);

        $cnParts = explode('\\',$linkedModel);
        $mName = $cnParts[count($cnParts)-1];
        $mt = $data->GetMetaData();

        $colInfo = [];
        foreach ($mt['ColumnInfo'] as $key => $spec)
            $colInfo[$key] = ['type' => $spec['ptype'], 'length' => $spec['length']];

        $outputs = [
            'summary' => 'Get list of '.$mName.' data.',
            'tags' => [
                strtolower($mName)
            ],
            'input_desc' => 'Filters, Orders, Offset, and Limit are optional. You can use one or more fields in Filters property to filter the data. You can use on or more fields in Orders property to sort the data. Filters use AND behaviour. Accepted value for any fields in Orders property are "asc" or "desc".',
            'input' => [
                'post' => [
                    'name' => ($mName.'Parameters'),
                    'type' => 'object',
                    'props' => [
                        'Filters' => [
                            'type' => 'object',
                            'props' => $colInfo
                        ],
                        'Orders' => [
                            'type' => 'object',
                            'props' => []
                        ],
                        'Offset' => [
                            'type' => 'int',
                            'length' => 10,
                        ],
                        'Limit' => [
                            'type' => 'int',
                            'length' => 10
                        ]
                    ]
                ]
            ],
            'output' => [
                '*' => [
                    'name' => ($mName.'Data'),
                    'type' => 'array',
                    'props' => $colInfo
                ]
            ]
        ];
        return $outputs;
    }

    public function _spec_action_create_data($params)
    {
        if (!isset($params['linked-model']))
            return [];

        $linkedModel = trim($params['linked-model']);
        if (!is_subclass_of($linkedModel,"RBS\\Selifa\\Data\\AbstractModel"))
            throw new Exception("'".$linkedModel."' does not inherit from AbstractModel.");

        $cnParts = explode('\\',$linkedModel);
        $mName = $cnParts[count($cnParts)-1];
        $specs = [
            'summary' => 'Insert new '.$mName.' entry.',
            'tags' => [strtolower($mName)]
        ];

        $model = call_user_func([$linkedModel,'CreateEmpty']);
        $rules = $model->GetValidationRules(MODEL_VALIDATION_TYPE_INSERT);

        if ($rules === null)
            $rules = [];

        if (count($rules) > 0)
        {
            $vald = new Validator($rules);
            $iSpecs = $vald->GenerateAPISpec();

            $iSpecs['name'] = ($mName.'InsertParameter');
            $specs['input'] = [
                'post' => $iSpecs
            ];
        }

        $specs['output'] = [
            '*' => [
                'outref' => ('Single'.$mName.'DataResponse')
            ]
        ];
        return $specs;
    }

    public function _spec_action_get_single($params)
    {
        if (!isset($params['linked-model']))
            return [];

        $linkedModel = trim($params['linked-model']);
        if (!is_subclass_of($linkedModel,"RBS\\Selifa\\Data\\AbstractModel"))
            throw new Exception("'".$linkedModel."' does not inherit from AbstractModel.");

        $model = call_user_func([$linkedModel,'CreateEmpty']);

        $cnParts = explode('\\',$linkedModel);
        $mName = $cnParts[count($cnParts)-1];

        $eData = $model->GetEmptyRowData();
        $mt = $eData->GetMetaData();

        $colInfo = [];
        foreach ($mt['ColumnInfo'] as $key => $spec)
            $colInfo[$key] = ['type' => $spec['ptype'], 'length' => $spec['length']];

        $outputs = [
            'summary' => 'Get single '.$mName.' data by id.',
            'tags' => [strtolower($mName)],
            'output' => [
                '*' => [
                    'name' => ('Single'.$mName.'Data'),
                    'type' => 'array',
                    'props' => $colInfo
                ]
            ]
        ];
        return $outputs;
    }

    public function _spec_action_edit($params)
    {
        if (!isset($params['linked-model']))
            return [];

        $linkedModel = trim($params['linked-model']);
        if (!is_subclass_of($linkedModel,"RBS\\Selifa\\Data\\AbstractModel"))
            throw new Exception("'".$linkedModel."' does not inherit from AbstractModel.");

        $cnParts = explode('\\',$linkedModel);
        $mName = $cnParts[count($cnParts)-1];
        $specs = [
            'summary' => 'Edit single '.$mName.' data.',
            'tags' => [strtolower($mName)]
        ];

        $model = call_user_func([$linkedModel,'CreateEmpty']);
        $rules = $model->GetValidationRules(MODEL_VALIDATION_TYPE_UPDATE);

        if ($rules === null)
            $rules = [];

        if (count($rules) > 0)
        {
            $vald = new Validator($rules);
            $iSpecs = $vald->GenerateAPISpec();

            $iSpecs['name'] = ($mName.'UpdateParameter');
            $specs['input'] = [
                'post' => $iSpecs
            ];
        }

        $specs['output'] = [
            '*' => [
                'outref' => ('Single'.$mName.'DataResponse')
            ]
        ];
        return $specs;
    }

    public function _spec_action_delete($params)
    {
        if (!isset($params['linked-model']))
            return [];

        $linkedModel = trim($params['linked-model']);
        if (!is_subclass_of($linkedModel,"RBS\\Selifa\\Data\\AbstractModel"))
            throw new Exception("'".$linkedModel."' does not inherit from AbstractModel.");

        $cnParts = explode('\\',$linkedModel);
        $mName = $cnParts[count($cnParts)-1];

        $outputs = [
            'summary' => 'Delete single '.$mName.' data.',
            'tags' => [strtolower($mName)],
            'output' => [
                '*' => [
                    'outref' => 'SelifaBaseResponse'
                ]
            ]
        ];
        return $outputs;
    }
}
?>