<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Data;
use RBS\Selifa\Data\AbstractModel;
use Exception;
use RBS\Selifa\Miria\RequestBody;
use RBS\Utility\Validation\Validator;

/**
 * Trait SLDCRUDController
 * Common endpoints for single level detail CRUD. *
 *
 * @package RBS\Utility\Data
 */
trait SLDCRUDController
{
    use SpecificationHandlerForCRUDController;

    /**
     * @var string
     */
    protected $ModelClassName = '';

    /**
     * @var string
     */
    protected $ParentModelClassName = '';

    /**
     * @var string
     */
    protected $ParentIDColumnKey = 'ParentID';

    /**
     * @param string $modelFQCN
     * @param string $parentModelFQCN
     * @param string $parentIdColumnKey
     * @throws Exception
     */
    protected function InitializeDefaultCRUD($modelFQCN,$parentModelFQCN,$parentIdColumnKey)
    {
        if (!is_subclass_of($modelFQCN,"RBS\\Selifa\\Data\\AbstractModel"))
            throw new Exception("'".$modelFQCN."' does not inherit from AbstractModel.");
        $this->ModelClassName = $modelFQCN;

        if (!is_subclass_of($parentModelFQCN,"RBS\\Selifa\\Data\\AbstractModel"))
            throw new Exception("'".$parentModelFQCN."' does not inherit from AbstractModel.");
        $this->ParentModelClassName = $parentModelFQCN;

        $this->ParentIDColumnKey = $parentIdColumnKey;
    }

    /**
     * @param string $methodName
     * @param array $params
     * @throws Exception
     */
    public function InitTrait_SLDCRUDController($methodName,$params)
    {
        if (isset($params['linked-model']))
        {
            $linkedModel = trim($params['linked-model']);
            if (!is_subclass_of($linkedModel,"RBS\\Selifa\\Data\\AbstractModel"))
                throw new Exception("'".$linkedModel."' does not inherit from AbstractModel.");
            $this->ModelClassName = $linkedModel;
        }

        if (isset($params['parent-model']))
        {
            $parentModel = trim($params['parent-model']);
            if (!is_subclass_of($parentModel,"RBS\\Selifa\\Data\\AbstractModel"))
                throw new Exception("'".$parentModel."' does not inherit from AbstractModel.");
            $this->ParentModelClassName = $parentModel;
        }

        if (isset($params['parent-id-key']))
            $this->ParentIDColumnKey = trim($params['parent-id-key']);
    }

    public function action_get_data($parentId)
    {
        if (!is_subclass_of($this->ModelClassName,"RBS\\Selifa\\Data\\IStaticGetRows"))
            throw new Exception("'".$this->ModelClassName."' does not implements IStaticGetRows.");

        $filters = RequestBody::GetData('Filters',[]);
        $orders = RequestBody::GetData('Orders',[]);
        $offset = (int)RequestBody::GetData('Offset',0);
        $limit = (int)RequestBody::GetData('Limit',0);

        $filters[$this->ParentIDColumnKey] = $parentId;
        $data = call_user_func([$this->ModelClassName,'GetRows'],$filters,$orders,$offset,$limit);
        return [
            'Data' => $data
        ];
    }

    public function action_create_data($parentId)
    {
        $model = call_user_func([$this->ModelClassName,'CreateEmpty']);
        if (!($model instanceof AbstractModel))
            throw new Exception("'".$this->ModelClassName."' does not inherit from AbstractModel.");

        $data = RequestBody::Instance();
        $data[$this->ParentIDColumnKey] = $parentId;

        $model->InsertFields($data);

        $newId = $model->GetID();
        $niModel = call_user_func([$this->ModelClassName,'ByID'],$newId);
        return [
            'Data' => $niModel->GetData()
        ];
    }

    public function action_get_single($parentId,$id)
    {
        $model = call_user_func([$this->ModelClassName,'ByID'],$id);
        if (!($model instanceof AbstractModel))
            throw new Exception("'".$this->ModelClassName."' does not inherit from AbstractModel.");
        return [
            'Data' => $model->GetData()
        ];
    }

    public function action_edit($parentId,$id)
    {
        $model = call_user_func([$this->ModelClassName,'ByID'],$id);
        if (!($model instanceof AbstractModel))
            throw new Exception("'".$this->ModelClassName."' does not inherit from AbstractModel.");

        $rules = $model->GetValidationRules(MODEL_VALIDATION_TYPE_UPDATE);
        if ($rules !== null)
        {
            $v = new Validator($rules);
            $dataToUpdate = $v->Validate(RequestBody::Instance());
        }
        else
            $dataToUpdate = RequestBody::Instance()->ToArray();

        $row = $model->UpdateFields($dataToUpdate);
        return [
            'Data' => $row
        ];
    }

    public function action_delete($parentId,$id)
    {
        $model = call_user_func([$this->ModelClassName,'ByID'],$id);
        if (!($model instanceof AbstractModel))
            throw new Exception("'".$this->ModelClassName."' does not inherit from AbstractModel.");

        $model->Delete();
    }
}
?>