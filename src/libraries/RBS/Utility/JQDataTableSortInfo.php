<?php
namespace RBS\Utility;

/**
 * Class JQDataTableSortInfo
 * @package RBS\Utility
 */
class JQDataTableSortInfo
{
    /**
     * @var int
     */
    public $ColumnIndex = -1;

    /**
     * @var string
     */
    public $ColumnName = '';

    /**
     * @var bool
     */
    public $IsSortable = false;

    /**
     * @var string
     */
    public $SortDirection = 'ASC';

    /**
     * @param array $data
     * @param string $keyName
     * @param mixed|null $default
     * @return mixed|null
     */
    protected function Extract($data, $keyName, $default = null)
    {
        if (isset($data[$keyName]))
            return $data[$keyName];
        return $default;
    }

    /**
     * @param array $postData
     * @param string $cIndex
     */
    public function __construct($postData, $cIndex)
    {
        $sortCol = (int)$this->Extract($postData, 'iSortCol_' . $cIndex, 0);
        $this->ColumnIndex = $sortCol;
        $this->ColumnName = $this->Extract($postData, 'mDataProp_' . $sortCol, '');
        $this->IsSortable = (boolean)$this->Extract($postData, 'bSortable_' . $sortCol, false);
        $this->SortDirection = strtoupper($this->Extract($postData, 'sSortDir_' . $cIndex, ''));
    }
}
?>