<?php
namespace RBS\Utility;

use RBS\Selifa\Data\BaseDatabaseDriver;
use RBS\Selifa\Data\BaseDialect;
use RBS\Selifa\Data\SQL\ConditionMember;
use RBS\Selifa\Data\SQL\QueryConditions;
use RBS\Selifa\Data\SQL\QueryObject;
use RBS\Selifa\Data\SQL\RowLimit;
use RBS\Selifa\Data\SQL\RowOrder;
use RBS\Selifa\Data\SQL\RowOrderMember;

define('SQL_DATATABLE_TOTAL_COUNT', '__DT__01');
define('SQL_DATATABLE_FILTERED_COUNT', '__DT__02');
define('SQL_DATATABLE_DATA', '__DT__03');

/**
 * Class JQDataTableParameter
 * @package RBS\Utility
 */
class JQDataTableParameter
{
    /**
     * @var string
     */
    public $Echo = '';

    /**
     * @var int
     */
    public $DisplayStart = 0;

    /**
     * @var int
     */
    public $DisplayLength = 0;

    /**
     * @var int
     */
    public $ColumnCount = 0;

    /**
     * @var mixed|null|string
     */
    public $SearchText = '';

    /**
     * @var bool
     */
    public $IsRegexSearch = false;

    /**
     * @var int
     */
    public $SortCount = 0;

    /**
     * @var JQDataTableColumnInfo[]
     */
    public $Columns = array();

    /**
     * @var JQDataTableSortInfo[]
     */
    public $Sorts = array();

    /**
     * @param array $data
     * @param string $keyName
     * @param mixed|null $default
     * @return mixed|null
     */
    protected function Extract($data, $keyName, $default = null)
    {
        if (isset($data[$keyName]))
        {
            return $data[$keyName];
        }
        return $default;
    }

    /**
     * @param null|array $postData
     */
    public function __construct($postData=null)
    {
        if ($postData === null)
            $postData = $_POST;

        $this->Echo = $this->Extract($postData, 'sEcho', '');
        $this->ColumnCount = (int)$this->Extract($postData, 'iColumns', 0);
        $this->DisplayStart = (int)$this->Extract($postData, 'iDisplayStart', 0);
        $this->DisplayLength = (int)$this->Extract($postData, 'iDisplayLength', 0);
        $this->SearchText = $this->Extract($postData, 'aSearch', $this->Extract($postData,'sSearch',''));
        $this->IsRegexSearch = (boolean)$this->Extract($postData, 'bRegex', false);
        $this->SortCount = (int)$this->Extract($postData, 'iSortingCols', 0);

        for ($i = 0; $i < $this->ColumnCount; $i++)
            $this->Columns[] = new JQDataTableColumnInfo($postData, $i);

        for ($i = 0; $i < $this->SortCount; $i++)
            $this->Sorts[] = new JQDataTableSortInfo($postData, $i);
    }

    /**
     * @param array $aData
     * @param int $totalCount
     * @param int|null $filteredCount
     * @return array
     */
    public function GenerateResult($aData,$totalCount,$filteredCount=null)
    {
        if ($filteredCount === null)
            $filteredCount = $totalCount;

        return array(
            'sEcho' => $this->Echo,
            'iTotalRecords' => $totalCount,
            'iTotalDisplayRecords' => $filteredCount,
            'aaData' => $aData,
            'DisplayLength' => $this->DisplayLength,
            'DisplayStart' => $this->DisplayStart
        );
    }

    /**
     * @param BaseDialect $dialect
     * @param QueryObject $q
     * @param string $whichQuery
     * @return string
     */
    protected function ProcessParamToQueryObject($dialect,$q,$whichQuery)
    {
        $s = '';
        if ($whichQuery == SQL_DATATABLE_TOTAL_COUNT)
        {
            $q->EnableTotalCountMode = true;
            $s = $q->toSQLString($dialect,false);
            $q->EnableTotalCountMode = false;
        }
        else
        {
            if (($whichQuery == SQL_DATATABLE_FILTERED_COUNT) || ($whichQuery == SQL_DATATABLE_DATA))
            {
                $newCondition = new QueryConditions();
                $newCondition->SetComparison(SQL_COMPARISON_OR);

                foreach ($this->Columns as $colInfo)
                {
                    if ($colInfo->IsSearchable)
                    {
                        if ($colInfo->SearchText != '')
                            $searchText = $colInfo->SearchText;
                        else
                            $searchText = $this->SearchText;

                        if ($searchText != '')
                        {
                            $cObject = $q->FindColumn($colInfo->ColumnName);
                            if ($cObject != null)
                            {
                                $ncMember = new ConditionMember($cObject, $searchText);
                                $ncMember->SetOperator(SQL_OPERATOR_LIKE);
                                $newCondition->AddMember($ncMember);
                            }
                        }
                    }
                    $cName[] = $colInfo->ColumnName;
                }

                $tcMode = ($whichQuery == SQL_DATATABLE_FILTERED_COUNT);
                $q->EnableTotalCountMode = $tcMode;

                $oldCondition = $q->GetCondition();
                if ($oldCondition != null)
                {
                    if ($newCondition->GetMemberCount() > 0)
                    {
                        $temp = new QueryConditions($oldCondition, $newCondition);
                        $q->SetCondition($temp);
                    }
                }

                if ($whichQuery == SQL_DATATABLE_DATA)
                {
                    $newRowOrder = new RowOrder();
                    foreach ($this->Sorts as $sortInfo)
                    {
                        if ($sortInfo->IsSortable)
                        {
                            $cObject = $q->FindColumn($sortInfo->ColumnName);
                            if ($cObject != null)
                            {
                                if ($sortInfo->SortDirection == 'ASC')
                                    $sd = 'Ascending';
                                else
                                    $sd = 'Descending';
                                $nroMember = new RowOrderMember($cObject, $sd);
                                $newRowOrder->AddMember($nroMember);
                            }
                        }
                    }

                    if ($newRowOrder->GetMemberCount() > 0)
                    {
                        $q->SetOrder($newRowOrder);
                    }

                    $limitObject = new RowLimit($this->DisplayLength, $this->DisplayStart);
                    $q->SetLimit($limitObject);
                }

                $s = $q->toSQLString($dialect,false);
                if ($oldCondition != null)
                    $q->SetCondition($oldCondition);
                else
                    $q->SetCondition(null);

                if ($tcMode)
                    $q->EnableTotalCountMode = false;
                if ($whichQuery == SQL_DATATABLE_DATA)
                {
                    $q->SetOrder(null);
                    $q->SetLimit(null);
                }
            }
        }
        return $s;
    }

    /**
     * @param BaseDatabaseDriver $driver
     * @param QueryObject $qObject
     * @param bool $returnQueries
     * @return array
     */
    public function GenerateResultFromQueryObject($driver,$qObject,$returnQueries=false)
    {
        $tcQuery = $this->ProcessParamToQueryObject($driver->Dialect,$qObject,SQL_DATATABLE_TOTAL_COUNT);
        $tcData = $driver->Prepare($tcQuery)->GetData()->FirstRow();

        $fcQuery = $this->ProcessParamToQueryObject($driver->Dialect,$qObject,SQL_DATATABLE_FILTERED_COUNT);
        $fcData = $driver->Prepare($fcQuery)->GetData()->FirstRow();

        $sql = $this->ProcessParamToQueryObject($driver->Dialect,$qObject,SQL_DATATABLE_DATA);
        $data = $driver->Prepare($sql)->GetData();

        $queries = array();
        if ($returnQueries)
        {
            $queries = array(
                'TC' => $tcQuery,
                'FC' => $fcQuery,
                'D' => $sql
            );
        }
        return array(
            'sEcho' => $this->Echo,
            'iTotalRecords' => (int)$tcData->TotalCount,
            'iTotalDisplayRecords' => (int)$fcData->TotalCount,
            'aaData' => $data->ToArray(),
            'DisplayLength' => $this->DisplayLength,
            'DisplayStart' => $this->DisplayStart,
            'Queries' => $queries
        );
    }
}
?>