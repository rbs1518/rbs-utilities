<?php
namespace RBS\Utility;
use RBS\Selifa\Data\BaseDialect;
use RBS\Selifa\Data\SQL;
use RBS\Selifa\Data\SQL\TableObject;
use RBS\Selifa\Data\SQL\QueryConditions;
use RBS\Selifa\Data\SQL\ConditionMember;
use RBS\Selifa\Data\SQL\QueryObject;

/**
 * Class JQQueryBuilderConverter
 * @package RBS\Utility
 */
class JQQueryBuilderConverter
{
    /**
     * @var null|array
     */
    private $_RawRules = null;

    /**
     * @var null|BaseDialect
     */
    private $_Dialect = null;

    /**
     * JQQueryBuilderConverter constructor.
     * @param array $qbRules
     * @param BaseDialect $dialect
     */
    public function __construct($qbRules,$dialect)
    {
        $this->_RawRules = $qbRules;
        $this->_Dialect = $dialect;
    }

    /**
     * @param string $input
     * @return string
     */
    private function __convertOperator($input)
    {
        switch ($input)
        {
            case 'equal': return SQL_OPERATOR_EQUAL;
            case 'not_equal': return SQL_OPERATOR_NOT_EQUAL;
            case 'in': return SQL_OPERATOR_IN;
            case 'not_in' : return '';
            case 'less': return SQL_OPERATOR_LESS_THAN;
            case 'less_or_equal': return SQL_OPERATOR_LESS_THAN_EQUAL;
            case 'greater': return SQL_OPERATOR_GREATER_THAN;
            case 'greater_or_equal': return SQL_OPERATOR_GREATER_THAN_EQUAL;
            case 'between': return SQL_OPERATOR_BETWEEN;
            case 'not_between': return SQL_OPERATOR_NOT_BETWEEN;
            case 'begins_with': return SQL_OPERATOR_BEGINS_WITH;
            case 'not_begins_with': return SQL_OPERATOR_NOT_BEGINS_WITH;
            case 'contains': return SQL_OPERATOR_LIKE;
            case 'not_contains': return SQL_OPERATOR_NOT_LIKE;
            case 'ends_with': return SQL_OPERATOR_ENDS_WITH;
            case 'not_ends_with': return SQL_OPERATOR_NOT_ENDS_WITH;
            case 'is_null': return SQL_OPERATOR_IS_NULL;
            case 'is_not_null': return SQL_OPERATOR_IS_NOT_NULL;
            case 'is_empty': return SQL_OPERATOR_IS_EMPTY;
            case 'is_not_empty': return SQL_OPERATOR_IS_NOT_EMPTY;
            default: return SQL_OPERATOR_EQUAL;
        }
    }

    /**
     * @param TableObject $dummy
     * @param QueryConditions $conds
     * @param array $items
     */
    private function __convertRules($dummy,$conds,$items)
    {
        foreach ($items as $item)
        {
            if (isset($item['condition']))
            {
                $newConds = SQL::Condition();
                $this->__convertRules($dummy,$newConds,$item['rules']);
                $newConds->SetComparison(strtoupper(trim($item['condition'])));
                $conds->AddMember($newConds);
            }
            else
            {
                $condition = new ConditionMember();
                $condition->AddActor($dummy->{$item['field']});
                $condition->AddActor($item['value']);

                $operator = strtolower(trim($item['operator']));
                $condition->SetOperator($this->__convertOperator($operator));
                $conds->AddMember($condition);
            }
        }
    }

    /**
     * @param string $tableAlias
     * @param bool $toString
     * @return QueryObject|string
     */
    public function Convert($tableAlias='',$toString=false)
    {
        $table = SQL::Table('Dummy',$tableAlias)->AddColumn('*');
        $conds = SQL::Condition();
        $this->__convertRules($table,$conds,array($this->_RawRules));

        $query = SQL::Query($table);
        $query->SetCondition($conds);
        if ($toString)
            return $query->toSQLString($this->_Dialect,false,true);
        else
            return $query;
    }
}
?>