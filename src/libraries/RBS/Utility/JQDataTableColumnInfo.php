<?php
namespace RBS\Utility;

/**
 * Class JQDataTableColumnInfo
 * @package RBS\Utility
 */
class JQDataTableColumnInfo
{
    /**
     * @var string
     */
    public $ColumnName = '';

    /**
     * @var string
     */
    public $SearchText = '';

    /**
     * @var bool
     */
    public $IsRegexSearch = false;

    /**
     * @var bool
     */
    public $IsSearchable = false;

    /**
     * @var bool
     */
    public $IsSortable = false;

    /**
     * @param array $data
     * @param string $keyName
     * @param mixed|null $default
     * @return mixed|null
     */
    protected function Extract($data, $keyName, $default = null)
    {
        if (isset($data[$keyName]))
            return $data[$keyName];
        return $default;
    }

    /**
     * @param array $postData
     * @param string $cIndex
     */
    public function __construct($postData, $cIndex)
    {
        $this->ColumnName = $this->Extract($postData, 'mDataProp_' . $cIndex, '');
        $this->SearchText = $this->Extract($postData, 'sSearch_' . $cIndex, '');
        $this->IsRegexSearch = (boolean)$this->Extract($postData, 'bRegex_' . $cIndex, false);
        $this->IsSearchable = (boolean)$this->Extract($postData, 'bSearchable_' . $cIndex, false);
        $this->IsSortable = (boolean)$this->Extract($postData, 'bSortable_' . $cIndex, false);
    }
}