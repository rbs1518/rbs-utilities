<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility;
use RBS\Selifa\Miria\RESTAPIController;
use RBS\Selifa\Data\DBI;

/**
 * Class RBSExtendedAPIController
 *
 * @package RBS\Utility
 */
class RBSExtendedAPIController extends RESTAPIController
{
    /**
     * @var null|RBSDataExtension
     */
    protected $_DBExtension = null;

    /**
     * @param array $options
     */
    protected function OnInitializing($options)
    {
        parent::OnInitializing($options);
        $this->_DBExtension = new RBSDataExtension();
        DBI::RegisterExtension($this->_DBExtension);
    }

    /**
     * @param bool $isAuthenticated
     * @param bool $isAuthorized
     */
    protected function OnAfterAuthentication(&$isAuthenticated, &$isAuthorized)
    {
        parent::OnAfterAuthentication($isAuthenticated, $isAuthorized);
        if ($isAuthenticated)
            $this->_DBExtension->SetUserID($this->UserID);
    }
}
?>